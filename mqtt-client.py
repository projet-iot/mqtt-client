#!/usr/bin/env python3

import paho.mqtt.client as mqtt
import sqlite3
import datetime

# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))

    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    client.subscribe("chair/+")

# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):
    chair_id = msg.topic.split("/")[1]
    
    try:
        chair_status = int(msg.payload)
        print("Chaise=" + str(chair_id) + " ; status=" + str(chair_status))
    
        cur = conn.cursor()
        try:
            if chair_status == 0: #le siège vient de se libérer
                cur.execute("UPDATE device SET lastUp = ? WHERE id = ?", (datetime.datetime.now(), chair_id,))
            else: #la personne vient de s'assoir
                cur.execute("UPDATE device SET currentSit = ? WHERE id = ?", (datetime.datetime.now(), chair_id,))
            conn.commit()
        except sqlite3.Error as e:
            print("Erreur SQL : ", e.args[0])
    except ValueError:
        print("Le subscriber a recu un payload qui n'est pas un entier, le message est : " + str(msg.payload))

client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message

client.connect("mqtt-broker", 1883, 60)

conn = sqlite3.connect('./db.sqlite')


# Blocking call that processes network traffic, dispatches callbacks and
# handles reconnecting.
# Other loop*() functions are available that give a threaded interface and a
# manual interface.
client.loop_forever()
