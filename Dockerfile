FROM python:alpine

RUN apk update
RUN pip install --upgrade pip

WORKDIR /app
COPY requirements.txt .
RUN pip install -r requirements.txt

COPY mqtt-client.py .

CMD python3 -u mqtt-client.py
